// listaEnlazadaStruct.cpp: define el punto de entrada de la aplicación de consola.
//

#include "stdafx.h"


#include <iostream>
#include <stdlib.h>
#include "conio.h"
#include <string>

using namespace std;

struct Node
{
	int n;
	struct Node *next;
};

void initInsert(Node *&init, int n)
{
	struct Node *data = NULL;
	data = new Node;
	data->n = n;
	data->next = NULL;

	if (init == NULL) init = data;
	else
	{
		data->next = init;
		init = data;
	}
}

void endInsert(Node *&init, int n)
{
	struct Node *aux = NULL;
	struct Node *data = NULL;
	data = new Node;
	data->n = n;
	data->next = NULL;

	aux = init;

	if (aux == NULL)
	{
		init = data;
	}
	else
	{
		while (aux->next != NULL) aux = aux->next;
		aux->next = data;
		cout << aux->next->n;
	}
}

void orderInsertRecursive(Node *&node, int n) {
	bool exit = false;
	struct Node *data = NULL;
	data = new Node;
	data->n = n;
	data->next = NULL;
	if (node == NULL) {
		
		data->next = NULL;

		node = data;
	}
	else
	{
		if (n < node->n)
		{
			data->next = node;
			node = data;
			exit = true;
		}
		else
		{
			if (n >= node->n && node->next == NULL)
			{
				node->next = data;
				exit = true;
			}
			else if (n >= node->n && n < node->next->n)
			{
				data->next = node->next;
				node->next = data;
				exit = true;
			}
		}
		if (!exit) orderInsertRecursive(node->next, n);
	}
}

void orderInsert(Node *&node, int n)
{
	struct Node *aux = NULL;
	struct Node *data = NULL;
	bool exitWhile = false;
	data = new Node;
	data->n = n;
	data->next = NULL;

	aux = node;

	if (aux == NULL) node = data;
	else
	{
		while (!exitWhile)
		{
			if (n < aux->n)
			{
				data->next = aux;
				node = data;
				exitWhile = true;
			}
			else
			{
				if (n >= aux->n && aux->next == NULL)
				{
					aux->next = data;
					exitWhile = true;
				}
				else if (n >= aux->n && n < aux->next->n)
				{
					data->next = aux->next;
					aux->next = data;
					exitWhile = true;
				}
			}
			aux = aux->next;
		}
	}
}

int deleteNode(Node *&node, int n) {
	//En vez que eliminara la posicion, elimine el numero(Falla)
	bool isNull = false;
	struct Node *aux = node;
	struct Node *before = NULL;
	if (aux == NULL) return -1;
	else
	{
		while (aux != NULL)
		{
			if (aux->n == n)
			{
				if (before == NULL && aux->next == NULL)
				{
					aux = NULL;
					isNull = true;
				}
				else
				{
					if (before == NULL && aux->next != NULL) aux = aux->next;
					else if (before != NULL && aux->next == NULL) before->next = NULL;
					else before->next = aux->next;
				}
			}
			before = aux;
			if(!isNull) aux = aux->next;
		}
		return 0;
	}
}

int recursiveDeleteNode(Node *&node, Node *before, int n) {
	if (node == NULL) return -1;
	else
	{
		if (node->n == n) {
			if (before == NULL && node->next == NULL) {
				node = NULL;
			}
			else {
				if (before == NULL && node->next != NULL) node = node->next;
				else if (before != NULL && node->next == NULL) before->next = NULL;
				else before->next = node->next;
			}
		}
		before = node;
		if (node != NULL) {
			if (node->next != NULL) {
				//cout << "fuck this shit";
				recursiveDeleteNode(node->next, before, n);
			}
		}
	}
}

void showList(Node *node)
{
	struct Node *aux;
	aux = node;
	if (aux != NULL)
	{
		while (aux != NULL)
		{
			cout << aux->n
				<< endl;
			aux = aux->next;
		}
		system("PAUSE");
		cin.get();
	}
	else
	{
		cout << "La lista esta vacia"
			<< endl;
		system("PAUSE");
		cin.get();
	}
}

void showRecursiveList(Node *node) {
	if (node != NULL) {
		cout << node->n << endl;
		if (node->next != NULL) showRecursiveList(node->next);
		system("PAUSE");
	}
	else {
		cout << "La lista esta vacia"
			<< endl;
		system("PAUSE");
	}
}

int main()
{
	struct Node *init = NULL;
	char userOption = 'f';
	int userNumber;
	while (userOption != 's')
	{
		system("cls");
		cout << "a - Agregar un numero"
			<< endl
			<< "b - Borrar una posicion de la lista"
			<< endl
			<< "m - Mostrar Lista"
			<< endl
			<< "s - Salir"
			<< endl;
		cin >> userOption;

		switch (userOption)
		{
		case 'a':
			system("cls");
			cout << "Digite un numero a guardar"
				<< endl;
			cin >> userNumber;
			orderInsertRecursive(init, userNumber);
			break;
		case 'b':
			system("cls");
			cout << "Digite el numero a borrar"
				<< endl;
			cin >> userNumber;
			switch (recursiveDeleteNode(init, NULL,userNumber))
			{
			case -1:
				system("cls");
				cout << "La lista esta vacia"
					<< endl;
				cin.get();
				break;
			case 0:
				system("cls");
				cout << "No existe el numero digitado"
					<< endl;
				cin.get();
				break;
			case 1:
				cout << "Se borro exitosamente";
				cin.get();
				break;
			}
			break;
		case 'm':
			system("cls");
			showList(init);
			break;
		}
	}

	return 0;
}

